#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib

class GoogleTTSOpener(urllib.FancyURLopener):
    version = 'Mozilla/5.0'
    def open(self, text, language = 'en'):
        url = 'http://translate.google.com/translate_tts'
        return urllib.FancyURLopener.open(self, '%s?tl=%s&q=%s' % (url,
            language, urllib.quote(text)))

if __name__ == '__main__':

    import pygame, time

    tts = GoogleTTSOpener()

    lang = language = 'en'
    #~_freq = 16000

    #~_pygame.mixer.pre_init(freq)
    pygame.init()

    while True:
        lang = raw_input('Language [%s]: ' % language)
        text = raw_input('Phrase [enter->exit]: ')

        if text == '':
            break

        if lang != '':
            #~_if lang != language and 'en' in (lang, language):
                #~_if lang == 'en':
                    #~_freq = 16000
                #~_elif language == 'en':
                    #~_freq = 22050
                #~_pygame.mixer.quit()
                #~_pygame.mixer.init(freq)
            language = lang

        try:
            speech = tts.open(text, language)

            f = open('speech.mp3', 'w')
            f.write(speech.read())
            f.close()

            speech.close()

            pygame.mixer.music.load('speech.mp3')
            pygame.mixer.music.play()

            while pygame.mixer.music.get_busy():
                time.sleep(1)
        except:
            print 'Error...'

    pygame.quit()
